﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Net;
using System.Text.RegularExpressions;

namespace vxLivestreamerGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string strCmdText = "twitch.tv/";

        string twitchApiURL = "https://api.twitch.tv/kraken/streams/";
        string pat = "\"game\":\"(.*?)\"";

        private void Form1_Load(object sender, EventArgs e)
        {
            ThreadStart ts = delegate { this.drawListWithOnlineStatus(); };
            Thread t = new Thread(ts);
            t.Start();
        }

        private void addFavButton_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && !listBox1.Items.Contains(textBox1.Text))
            {
                listBox1.Items.Add(textBox1.Text);
                List<string> streams = new List<string>();
                foreach (string item in listBox1.Items)
                {
                    streams.Add(item);
                }
                saveStreams(streams);
                
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
                List<string> streams = new List<string>();
                foreach (string item in listBox1.Items)
                {
                    streams.Add(item);
                }
                saveStreams(streams);
            }
        }

        private void saveStreams(List<string> streams)
        {
            StringBuilder file = new StringBuilder("");
            foreach (string item in streams)
            {
                file.AppendLine(extractName(item));
            }
            File.WriteAllText("streams.txt", file.ToString());
        }

        private List<string> loadStreams()
        {
            if (!File.Exists("streams.txt"))
            {
                return null;
            }
            string[] file = File.ReadAllLines("streams.txt");
            List<string> result = new List<string>();
            foreach (string item in file)
            {
                result.Add(item);
            }
            return result;
        }

        private void lowButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                System.Diagnostics.Process.Start("livestreamer", strCmdText + extractName(listBox1.SelectedItem.ToString()) + " low");
            }
        }

        private void mediumButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                System.Diagnostics.Process.Start("livestreamer", strCmdText + extractName(listBox1.SelectedItem.ToString()) + " medium");
            }
        }

        private void highButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                System.Diagnostics.Process.Start("livestreamer", strCmdText + extractName(listBox1.SelectedItem.ToString()) + " high");
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                System.Diagnostics.Process.Start("livestreamer", strCmdText + extractName(listBox1.SelectedItem.ToString()) + " best");
            }
        }

        private void bestButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                System.Diagnostics.Process.Start("livestreamer", strCmdText + extractName(listBox1.SelectedItem.ToString()) + " best");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ThreadStart ts = delegate { this.drawListWithOnlineStatus(); };
            Thread t = new Thread(ts);
            t.Start();
        }

        public void drawListWithOnlineStatus()
        {
            listBox1.Items.Clear();
            List<String> streams = loadStreams();
            if (streams != null)
            {
                foreach (string item in streams)
                {
                    string finalText = item;
                    // add call to twitch API
                    try
                    {
                        string streamerApiUrl = twitchApiURL + item;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(streamerApiUrl);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        Stream objStream = null;
                        if (response != null)
                        objStream = response.GetResponseStream();
                        
                        StreamReader objReader = null;
                        if (objStream != null)
                        objReader = new StreamReader(objStream);

                        string json = "";
                        string sLine = "";
                        int i = 0;

                        while (sLine != null)
                        {
                            i++;
                            sLine = objReader.ReadLine();
                            json += sLine;
                        }
                        
                        if (json.Contains("does not exist"))
                        {
                            finalText = "(unknown) " + item;
                        }
                        else if (!json.Contains("\"stream\":null"))
                        {
                            finalText = "(LIVE) " + item;
                            Regex r = new Regex(pat, RegexOptions.IgnoreCase);

                            // Match the regular expression pattern against a text string.
                            Match m = r.Match(json);
                            if (m.Success)
                            {
                                Group g = m.Groups[1];
                                finalText += " - " + g;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        finalText = "(ERROR) " + item;
                    }
                    listBox1.Items.Add(finalText);
                }
            }

            if (listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = 0;
            }
        }

        public string extractName(string listBoxItem)
        {
            string[] list = listBoxItem.Split(' ');
            if (list.Length > 1)
            {
                return list[1];
            }
            else
            {
                return list[0];
            }
        }
    }
}
